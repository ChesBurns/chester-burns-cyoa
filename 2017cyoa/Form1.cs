﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2017cyoa
{

    public partial class Form1 : Form
    {

        public struct Room
        {
            public int roomID;
            public string story;
            public int opt1;
            public string opt1Text;
            public int opt2;
            public string opt2Text;
            public int opt3;
            public string opt3Text;
        }


        Room[] room = new Room[19]; 

        public Form1()
        {
            InitializeComponent();

            this.btn2.Click += btn1_Click;
            this.btn3.Click += btn1_Click;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            readFile();

            loadRoom(0);
        }

        private void readFile()
        {
            string line;

            int counter = 0;

            System.IO.StreamReader file =
               new System.IO.StreamReader(Application.StartupPath + "\\story.txt");
            while ((line = file.ReadLine()) != null)
            {
               
                string[] tmp = line.Split('|');

                room[counter].roomID = int.Parse(tmp[0]);
                room[counter].story = tmp[1];
                room[counter].opt1 = int.Parse(tmp[2]);
                room[counter].opt1Text = tmp[3];
                room[counter].opt2 = int.Parse(tmp[4]);
                room[counter].opt2Text = tmp[5];
                room[counter].opt3 = int.Parse(tmp[6]);
                room[counter].opt3Text = tmp[7];
                counter++;
            }

            file.Close();
        }

        private void loadRoom(int roomID)
        {
            lblStory.Text = room[roomID].story;
            btn1.Text = room[roomID].opt1Text;

            btn1.Tag = room[roomID].opt1;
            btn2.Text = room[roomID].opt2Text;
            btn2.Tag = room[roomID].opt2;
            btn3.Text = room[roomID].opt3Text;
            btn3.Tag = room[roomID].opt3;
        }


        private void btn1_Click(object sender, EventArgs e)
        {

            Button sentButton = (Button)sender;

            int ID = int.Parse(sentButton.Tag.ToString());

            loadRoom(ID);
        }

        private void lblStory_Click(object sender, EventArgs e)
        {

        }
    }
}
